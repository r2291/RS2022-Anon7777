#!/bin/bash
python3 generate_TRO_test_environments.py --length 400 --point_distance 1.0 --radius 0.20 --mode line
python3 generate_TRO_test_environments.py --length 400 --point_distance 1.0 --radius 0.20 --mode circle --period 5.0
python3 generate_TRO_test_environments.py --length 400 --point_distance 1.0 --radius 0.20 --mode circle --period 10.0
python3 generate_TRO_test_environments.py --length 400 --point_distance 1.0 --radius 0.20 --mode circle --period 15.0
python3 generate_TRO_test_environments.py --length 400 --point_distance 1.0 --radius 0.20 --mode circle --period 20.0
python3 generate_TRO_test_environments.py --length 400 --point_distance 1.0 --radius 0.20 --mode circle --period 25.0
python3 generate_TRO_test_environments.py --length 400 --point_distance 1.0 --radius 0.20 --mode circle --period 30.0
python3 generate_TRO_test_environments.py --length 400 --point_distance 1.0 --radius 0.20 --mode circle --period 40.0
python3 generate_TRO_test_environments.py --length 400 --point_distance 1.0 --radius 0.20 --mode saw --period 5.0
python3 generate_TRO_test_environments.py --length 400 --point_distance 1.0 --radius 0.20 --mode saw --period 10.0
python3 generate_TRO_test_environments.py --length 400 --point_distance 1.0 --radius 0.20 --mode saw --period 15.0
python3 generate_TRO_test_environments.py --length 400 --point_distance 1.0 --radius 0.20 --mode saw --period 20.0
python3 generate_TRO_test_environments.py --length 400 --point_distance 1.0 --radius 0.20 --mode saw --period 25.0
python3 generate_TRO_test_environments.py --length 400 --point_distance 1.0 --radius 0.20 --mode saw --period 30.0
python3 generate_TRO_test_environments.py --length 400 --point_distance 1.0 --radius 0.20 --mode saw --period 40.0