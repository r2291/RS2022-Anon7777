#!/bin/bash
model_path=$1
env_path=$2
ros_workspace=$3
save_dir=$4

source $ros_workspace

model_list=$(ls $model_path)
env_list=$(ls $env_path)

mkdir $save_dir
for model in $model_list
do
    mkdir $save_dir/$model
    for env in $env_list
    do
        mkdir $save_dir/$model/$env
        mode=$(echo $env| cut -d'_' -f 1)
        if [ $mode = "line" ]
        then
            period=-1
        else
            period=$(echo $env| cut -d'_' -f 5)
        fi
        roslaunch uuv_gazebo_worlds TRO_test_env.launch world_name:=$env &
        sleep 5
        roslaunch dreamer2ros imgphy2img_flex_evaluate.launch exp_path:=$save_dir/$model/$env model_path:=$model_path/$model &
        sleep 10
        roslaunch rl_server usv_flex_following_eval.launch x:=10 y:=10 yaw:=0 mode:=$mode period:=$period
        sleep 5
        killall gzclient
        kill rosmaster
        killall roscore


    done
done
