#!/usr/bin/python3
import numpy as np
import argparse

def parseArgs():
    parser = argparse.ArgumentParser()
    parser.add_argument("--length", type=int, default=1000)
    parser.add_argument("--mode", type=str, required=True)
    parser.add_argument("--period", type=float, default=10.0)
    parser.add_argument("--point_distance", type=float, default=1.0)
    parser.add_argument("--radius", type=float, default=0.25)
    return parser.parse_args()

def sawTooth(x, T=10, alpha=1.0):
    if (x%T) < T/2:
        y = x%T*alpha
    else:
        y = -x%T*alpha
    return y

def makeCircularShape(p, R=10, d_points=0.5):
    c0 = (p*2*R*2,0)
    c1 = ((p*2 + 1)*R*2,0)
    half_perimeter = R*np.pi
    nb_points = int(half_perimeter//d_points)
    angles = np.flip(np.linspace(0,np.pi,nb_points)[:-1])
    x1 = R*np.cos(angles) + c0[0]
    y1 = R*np.sin(angles) + c0[1]
    x2 = R*np.cos(-angles) + c1[0]
    y2 = R*np.sin(-angles) + c1[1]
    x = np.concatenate([x1,x2],axis=-1)
    y = np.concatenate([y1,y2],axis=-1)
    return x + R, y

def buildArcLine(L=1000, R=10, d=1.0):
    x = []
    y = []
    p = int(L // (4*R))
    for i in range(p):
        xt,yt = makeCircularShape(p=i,R=R,d_points=d)
        x.append(xt)
        y.append(yt)
    x = np.concatenate(x,axis=-1,)
    y = np.concatenate(y,axis=-1)
    return x, y

def buildStraightLine(L=1000, d=1.0):
    x = np.linspace(0,L,int(L/d))
    y = np.zeros_like(x)
    return x,y

def buildSawToothLine(L=1000, T=10, d=1.0, alpha=1.0):
    step = np.sqrt(1 + alpha**2)
    point_per_period = int(T*step/d)
    if point_per_period %2 == 0:
        point_per_period += 1
    nb_period = int(L/T)
    total_points = nb_period * (point_per_period-1) + 1
    total_distance = nb_period*T
    x = np.linspace(0,total_distance,total_points)
    y = [sawTooth(i, T=T, alpha=alpha) for i in x]
    return x,y

def genPoints(mode, L, T, d=1.0):
    if mode == "circle":
        x,y = buildArcLine(L=L, R=T/4, d=d)
        name = "half_circles_"+str(L)+"m_period_"+str(T)+"_Pdist_"+str(d)
    elif mode == "saw":
        x,y = buildSawToothLine(L=L, T=T, d=d)
        name = "saw_tooth_"+str(L)+"m_period_"+str(T)+"_Pdist_"+str(d)
    elif mode == "line":
        x,y = buildStraightLine(L=L, d=d)
        name = "line_"+str(L)+"m_Pdist_"+str(d)
    else:
        raise ValueError("Requested mode: "+mode+" not supported")
    return x, y, name

def makeCylinder(x, y, radius, name):
    cylinder =['    <model name="'+name+'">',
                '      <pose frame="">'+str(x)+' '+str(y)+' 0.75 0 0 0</pose>'
                '      <static>1</static>',
                '      <link name="link">',
                '        <collision name="collision">',
                '          <geometry>',
                '            <cylinder>',
                '              <radius>'+str(radius)+'</radius>',
                '              <length>2</length>',
                '            </cylinder>',
                '          </geometry>',
                '          <surface>',
                '            <contact><ode/></contact>',
                '            <bounce/>',
                '            <friction>',
                '              <torsional><ode/></torsional>',
                '              <ode/>',
                '            </friction>',
                '          </surface>',
                '        </collision>',
                '        <visual name="cylinder">',
                '          <geometry>',
                '            <cylinder>',
                '              <radius>'+str(radius)+'</radius>',
                '              <length>2</length>',
                '            </cylinder>',
                '          </geometry>',
                '        </visual>',
                '      </link>',
                '    </model>']
    return cylinder

def read_sdf(path):
    with open(path, 'r') as file:
        basics = file.readlines()
    basics = [i.split('\n')[0] for i in basics]
    return basics

def write_world(data, path):
    with open(path, 'w') as file:
        file.writelines(data)

def genWorld(x, y, name, radius=0.25):
    world = []
    header = ['<?xml version="1.0" ?>','<sdf version="1.5">','<world name="'+name+'">']
    footer = ['</world>',"</sdf>"]
    world += header
    world += read_sdf("basics.world")
    world += ['  ','    <!--Automatically Generated World-->']
    for i,(x,y) in enumerate(zip(x,y)):
        world += makeCylinder(x, y, radius, name="cylinder_"+str(i))
    world += footer
    world = [i+"\n" for i in world]
    return world


if __name__ == "__main__":
    args = parseArgs()
    x, y, name = genPoints(args.mode, args.length, args.period, args.point_distance)
    print(x.shape, np.max(x))
    world = genWorld(x, y, name, args.radius)
    write_world(world, "envs/"+name+".world")
