#!/usr/bin/env python

import rospy
import json
import numpy as np
from std_msgs.msg import Bool, Float32, String
from rl_server.msg import Episode
from gazebo_msgs.msg import ModelState 
from gazebo_msgs.srv import SetModelState
from dreamer2ros.msg import Act2
import subprocess

import sampling_helper as sh

def sawTooth(x, T=10, alpha=1.0):
    if (x%T) < T/2:
        y = x%T*alpha
    else:
        y = -x%T*alpha
    return y

class Server:
    def __init__(self):
        # Publishers
        self.episode_manager_pub_ = rospy.Publisher('server/episode_manager', Episode, queue_size=1)  
        self.action_pub_ = rospy.Publisher('cmd_rl', Act2, queue_size=1)
        self.sim_ok_pub_ = rospy.Publisher('server/run_agent', Bool, queue_size=1)  
        self.exp_name_pub_ = rospy.Publisher('server/exp_name', String, queue_size=1)  
        self.stop_agent_ = rospy.Publisher('server/stop_agent', Bool, queue_size=1)  
        self.target_pub_ = rospy.Publisher('target_vel', Float32, queue_size=1)  
        # Subscribers
        rospy.Subscriber('agent/is_done', Bool, self.doneCallback)
        # Services
        self.spawn_service_ = rospy.get_param('~spawn_service','')
        # Experiment settings
        self.ideal_dist_ = rospy.get_param('~ideal_dist', 10.) + 0.6
        self.vsteps_ = [1.7,1.5,1.4,1.3,1.2,1.1,1.0,0.9,0.8,0.7,0.6,0.5,0.4,0.35,0.3,0.25,0.2,-0.2,-0.3,-0.4,-0.5,-0.6,-0.7]
        self.periods_ = [5.0,10.0,15.0,20.0,30.0]
        self.vmax_ = [1.5,1.5,1.3,1.0,1.0,0.6]
        self.vmin_ = [1.0,0.5,0.7,0.5,0.0,0.3]
        self.repetitions_ = 5
        self.rate_ = 12
        # others
        self.robot_name_ = rospy.get_param('~robot_name','')
        self.mode_ = rospy.get_param('~mode','')
        self.period_ = rospy.get_param('~period',-1)
        # Compute initial position
        if self.mode_ == "line":
            self.y = self.ideal_dist_
            self.x = 200
            self.theta = 0
        elif self.mode_ == "saw":
            self.x = 200
            self.y = self.ideal_dist_ + sawTooth(self.x, T=self.period_)
            if self.x%self.period_/2 != 0:
                if self.x%self.period_ > self.period_/2:
                    self.theta = np.pi/4
                else: 
                    self.theta = -np.pi/4
        elif self.mode_ == "half":
            self.x = 200
            self.y = self.ideal_dist_ + (np.sin(2*np.pi*self.x/self.period_) + 1)* self.period_
            self.theta = 0

        # Reset command
        self.make_reset_cmd()
        # Spawn
        self.usv_spawn_req = self.make_spawn_req(self.robot_name_)

    def make_reset_cmd(self):
        self.reset_cmd = Act2()
        self.reset_cmd.a0 = 0
        self.reset_cmd.a1 = 0

    def make_spawn_req(self, name):
        # SPAWN SERVICE
        pose_req_ = ModelState()
        pose_req_.model_name = name
        pose_req_.pose.position.x = 0
        pose_req_.pose.position.y = 0
        pose_req_.pose.position.z = 0.145
        pose_req_.pose.orientation.x = 0
        pose_req_.pose.orientation.y = 0
        pose_req_.pose.orientation.z = 0
        pose_req_.pose.orientation.w = 0
        pose_req_.twist.linear.x = 0
        pose_req_.twist.linear.y = 0
        pose_req_.twist.linear.z = 0
        pose_req_.twist.angular.x = 0
        pose_req_.twist.angular.y = 0
        pose_req_.twist.angular.z = 0
        return pose_req_

    def doneCallback(self, msg):
        if msg.data:
            self.op_OK_=True

    def applyFixedVelocity(self, vel, duration=40):
        r = rospy.Rate(self.rate_)
        timer_expired = False
        start = rospy.Time.now()
        while (not rospy.is_shutdown()) and (not timer_expired):
            if (rospy.Time.now() - start).to_sec() > duration:
                print((rospy.Time.now() - start).to_sec())
                timer_expired = True
            self.target_pub_.publish(vel)
            r.sleep()
    
    def applySinWave(self, vmax, vmin, period, duration=60):
        r = rospy.Rate(self.rate_)
        timer_expired = False
        start = rospy.Time.now()
        while (not rospy.is_shutdown()) and (not timer_expired):
            current_time = rospy.Time.now()
            if (current_time - start).to_sec() > duration:
                timer_expired = True
            vel = vmin + (vmax - vmin)*(np.sin(np.pi*2*current_time.to_sec()/period)+1)/2
            self.target_pub_.publish(vel)
            r.sleep()
    
    def applySawteeth(self, vmax, vmin, period, duration=60):
        r = rospy.Rate(self.rate_)
        timer_expired = False
        start = rospy.Time.now()
        while (not rospy.is_shutdown()) and (not timer_expired):
            current_time = rospy.Time.now()
            if (current_time - start).to_sec() > duration:
                timer_expired = True
            if (current_time.to_sec() % period) < period/2.0:
                vel = vmin + (vmax - vmin)*(current_time.to_sec()%period)*2/period
            else:
                vel = vmin + (vmax - vmin)*(-current_time.to_sec()%period)*2/period
            self.target_pub_.publish(vel)
            r.sleep()
    
    def applySteps(self, vmax, vmin, period, duration=60):
        r = rospy.Rate(self.rate_)
        timer_expired = False
        start = rospy.Time.now()
        while (not rospy.is_shutdown()) and (not timer_expired):
            current_time = rospy.Time.now()
            if (current_time - start).to_sec() > duration:
                timer_expired = True
            if (current_time.to_sec() % period) < period/2.0:
                vel = vmin
            else:
                vel = vmax
            self.target_pub_.publish(vel)
            r.sleep()

    def runFixedRegime(self):
        sleep = rospy.Duration.from_sec(2.0)
        rospy.loginfo("Starting fixed regime evaluation")
        for vel in self.vsteps_:
            rospy.loginfo("  Requesting "+str(vel)+"m/s")
            for i in range(self.repetitions_):
                self.spawnObject(self.usv_spawn_req, [self.x,self.y,0])
                self.action_pub_.publish(self.reset_cmd)
                rospy.sleep(sleep)
                self.spawnObject(self.usv_spawn_req, [self.x,self.y,0])
                self.action_pub_.publish(self.reset_cmd)
                rospy.sleep(sleep)
                self.exp_name_pub_.publish("fixed_regime_"+str(vel)+"_rep_"+str(i))
                self.sim_ok_pub_.publish(True)
                self.applyFixedVelocity(vel)
                self.stop_agent_.publish(True)
                self.action_pub_.publish(self.reset_cmd)
                rospy.loginfo("    Repetition "+str(i)+" done.") 
                rospy.sleep(sleep)
    
    def runWave(self, wave, name):
        sleep = rospy.Duration.from_sec(2.0)
        rospy.loginfo("Starting "+name+" velocity tracking")
        for vmax,vmin in zip(self.vmax_, self.vmin_):
            rospy.loginfo("  Requesting vmax:"+str(vmax)+"m/s, vmin: "+str(vmin)+"m/s")
            for period in self.periods_:
                rospy.loginfo("    Building "+name+" with period: "+str(period)+"s")
                for i in range(self.repetitions_):
                    self.spawnObject(self.usv_spawn_req, [self.x,self.y,0])
                    self.action_pub_.publish(self.reset_cmd)
                    rospy.sleep(sleep)
                    self.spawnObject(self.usv_spawn_req, [self.x,self.y,0])
                    self.action_pub_.publish(self.reset_cmd)
                    rospy.sleep(sleep)
                    self.exp_name_pub_.publish("wave_"+name+"_"+str(vmax)+"_"+str(vmin)+"_period_"+str(period)+"_rep_"+str(i))
                    self.sim_ok_pub_.publish(True)
                    wave(vmax,vmin,period)
                    self.stop_agent_.publish(True)
                    self.action_pub_.publish(self.reset_cmd)
                    rospy.loginfo("      Repetition "+str(i)+" done.") 
                    rospy.sleep(sleep)

    def run_experiment(self):
        rospy.loginfo("Starting Evaluation Benchmark!")
        rospy.loginfo("Spawning boat")
        self.spawnObject(self.usv_spawn_req, [self.x,self.y,self.theta])
        self.action_pub_.publish(self.reset_cmd)
        rospy.sleep(rospy.Duration.from_sec(10))
        self.runFixedRegime()
        self.runWave(self.applySteps, "square")
        self.runWave(self.applySinWave,"sinus")
        self.runWave(self.applySawteeth, "saw")
        rospy.sleep(rospy.Duration.from_sec(10))
        rospy.loginfo("Benchmark done!")

    def spawnObject(self, req, pos):
        try:
            set_state = rospy.ServiceProxy(self.spawn_service_, SetModelState)
            req.pose.position.x = pos[0]
            req.pose.position.y = pos[1]
            q = sh.euler2quat(pos[2])
            req.pose.orientation.x = q[0]
            req.pose.orientation.y = q[1]
            req.pose.orientation.z = q[2]
            req.pose.orientation.w = q[3]      
            resp = set_state(req)
            print("refresh Ok new boat pose: x:",pos[0]," y:",pos[1]," yaw:",pos[2])
        except rospy.ServiceException as e:
            print("Service call failed: %s" % e)
        rospy.sleep(1.0)

if __name__ == "__main__":
    rospy.init_node('server')
    server = Server()
    server.run_experiment()
    subprocess.Popen("killall -9 gzserver",shell=True)
    subprocess.Popen("rosnode kill -a",shell=True)
    #subprocess.Popen("kill rosmaster",shell=True) 
    #subprocess.Popen("killall roscore", shell=True)
    #subprocess.Popen("killall -9 roslaunch",shell=True) 

