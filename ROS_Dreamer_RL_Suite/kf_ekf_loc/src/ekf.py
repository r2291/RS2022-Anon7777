#!/usr/bin/python
import os
#we need that to avoid numpy calling openBLAS which is highly CPU extensive...
os.environ["OMP_NUM_THREADS"] = "1"
import rospy
from nav_msgs.msg import Odometry
from sensor_msgs.msg import NavSatFix, Imu
from geometry_msgs.msg import Vector3Stamped
import tf
import copy
import utm
import numpy as np
from math import cos, sin, atan2, pi, sqrt, hypot, fabs

class EKFLocalization:
    def __init__(self):
        #class members, initialization and flags
        self.rpy_imu = Vector3Stamped()
        self.rpy_rtk = Vector3Stamped()
        self.imu = Imu()
        self.rtk = NavSatFix()
        self.new_rtk = False
        self.new_imu = False
        self.new_rpy_imu = False
        self.new_rpy_rtk = False
        self.prev_imu = None
        self.prev_rpy_imu = None
        self.prev_rpy_rtk = None
        self.prev_rtk = None
        self.prev_time = None
        self.init_speed = None
        rospy.init_node('kf_ekf_loc')
        self.robot_frame = rospy.get_param("~robot_frame","base_link")
        self.odom_frame = rospy.get_param("~odom_frame","odom_ekf")
        self.gps_frame = rospy.get_param("~gps_frame","gps")#for the static transform between the position of the gps and base_link
        self.odom_as_parent = rospy.get_param("~odom_as_parent",False)#whether we publish the new odom frame as parent of base_link, default: as child
        #subscribers and publishers
        self.imu_sub = rospy.Subscriber('imu', Imu, self.imuCallback, queue_size=1)
        self.rpy_imu_sub = rospy.Subscriber('rpy_imu', Vector3Stamped, self.rpyImuCallback, queue_size=1)
        self.rpy_rtk_sub = rospy.Subscriber('rpy_rtk', Vector3Stamped, self.rpyRtkCallback, queue_size=1)
        self.rtk_sub = rospy.Subscriber('rtk', NavSatFix, self.rtkCallback, queue_size=1)
        self.fuse_odom_pub = rospy.Publisher('~fused_odom', Odometry, queue_size=1)
        self.speed_pub = rospy.Publisher('~robot_speed', Odometry, queue_size=1)
        self.broadcaster = tf.TransformBroadcaster()
        self.listener = tf.TransformListener()
        #we wait for the first messages to initialize the state:
        self.wait_for_init()
        self.rtk_init = self.get_baselink_position_from_rtk_in_utm(self.rtk)
        rpy_init = self.rpy_enu_from_ned(self.rpy_rtk.vector)[2]+pi/2
        #class members representing the state
        #we initialize the state with uncertainty 
        #state in 2d containing 7 variables: X, Y , YAW , VX, VY, VANG, + BIAS_GYRO
        self.X = np.vstack((0,0,rpy_init,self.init_speed[0],self.init_speed[1],self.imu.angular_velocity.z,0))
        self.P = np.diag((1e-2,1e-2,1e-2,1e-2, 1e-2, 1e-2,1e-2))
        self.Q = np.diag((1e-3,1e-3,1e-4,1e-4,1e-4,1e-3,1e-7))
        #Now that everything is ready, we can call the Callback on the wheel odometry:
        rospy.loginfo("initialization complete, Initial State is:" + str(self.X))

    #wait until we have all the necessary information to initalize the state
    def wait_for_init(self):
        rospy.loginfo("Waiting for first rtk, imu and first speed")
        rospy.sleep(.5) 
        while not rospy.is_shutdown():
            if ((self.rpy_rtk == None) or (self.rtk == None)) or (self.imu == None):
                continue
            else:
                if not self.can_init_speed():
                    continue
                else: 
                    return
    #check wethere we can calculate an inital speed from the rtk 
    def can_init_speed(self):
        if self.prev_rtk == None:
            self.prev_rtk = self.rtk
            return False
        dt = (self.rtk.header.stamp - self.prev_rtk.header.stamp).to_sec()
        [x,y,_] = self.get_baselink_position_from_rtk_in_utm(self.rtk)
        [x_prev,y_prev,_] = self.get_baselink_position_from_rtk_in_utm(self.prev_rtk)
        if not (dt < 1e-5):#(==0)
            vx = (x-x_prev)/dt 
            vy = (y-y_prev)/dt
            self.init_speed = [vx,vy]
            return True
        else: return False

    #prediction step of the EKF
    def predict_without_cmd(self):    
        if self.prev_time == None:
            self.prev_time = rospy.Time.now()
        dt = (rospy.Time.now()- self.prev_time).to_sec()
        self.prev_time = rospy.Time.now()
        #predict next state
        X_pred = np.array(([self.X[0,0] + self.X[3,0]*dt],
                            [self.X[1,0] + self.X[4,0]*dt],
                            [self.X[2,0] + self.X[5,0]*dt],
                            [self.X[3,0]],
                            [self.X[4,0]],
                            [self.X[5,0]],
                            [self.X[6,0]]))
        A = np.array(([1, 0, 0, dt,  0,  0,  0],
                        [0, 1, 0,  0, dt,  0, 0],
                        [0, 0, 1,  0,  0, dt, 0],
                        [0, 0, 0,  1,  0,  0, 0],
                        [0, 0, 0,  0,  1,  0, 0],
                        [0, 0, 0,  0,  0,  1, 0],
                        [0, 0, 0,  0,  0,  0, 1]))
        P_pred = np.matmul(np.matmul(A, self.P), np.transpose(A)) + self.Q
        return X_pred, P_pred
    
    #correction step based on the last sensors readings:
    def correct(self, X_pred, P_pred):
        Zk = np.empty([0,1])
        R_diag = np.empty([0,1])
        H = np.empty([0,7])
        #we correct based on which new sensor reading we have
        #observation of the angular speed from the imu
        if self.new_imu:
            #IMU is pointing down, we need to invert the sign
            Zk = np.vstack((Zk,-self.imu.angular_velocity.z))
            R_diag = np.vstack((R_diag, 0.035**2))#/5
            H = np.vstack((H,(0,0,0,0,0,1,1)))
        #observation of the yaw from the imu
        if self.new_rpy_imu:
            current_in_enu = self.rpy_enu_from_ned(self.rpy_imu.vector)[2] +pi/2
            Zk = np.vstack((Zk,current_in_enu))
            R_diag = np.vstack((R_diag, 0.25**2))#/5
            H = np.vstack((H,(0,0,1,0,0,0,0)))
        #observation of the position from the rtk
        if self.new_rtk:
            #convert the rtk lat,long into utm and into base_link
            [x, y, _] = self.get_baselink_position_from_rtk_in_utm(self.rtk)
            Zk = np.vstack((Zk, x-self.rtk_init[0], y-self.rtk_init[1]))
            if (self.rtk.status.status == 3):  # fix, we are confident
                R_diag = np.vstack((R_diag, 0.15**2, 0.15**2))
            elif (self.rtk.status.status == 2):  # float, we are  less confident
                R_diag = np.vstack((R_diag, 1**2, 1**2))
            else:  # bad, we don't trust it
                R_diag = np.vstack((R_diag, 5**2, 5**2))
            H = np.vstack((H, (1,0,0,0,0,0,0), (0,1,0,0,0,0,0)))
        #now, do the actual correction based on these observations
        R = np.diagflat(R_diag)
        K = np.matmul(np.matmul(P_pred, np.transpose(H)), np.linalg.inv(np.matmul(H, np.matmul(P_pred, np.transpose(H)))+R))
        self.P = copy.deepcopy(np.matmul(np.identity(7) - np.matmul(K, H), P_pred))
        Zi = Zk - np.matmul(H, X_pred)
        #if we update the yaw, make sure we deal with 2pi jumps
        if H.size != 0:
            if np.sum(H,axis=0)[2]:
                i = np.argwhere(H[:,2]==1)
                Zi[i,0] = np.fmod(Zi[i,0]+3*pi,2*pi)-pi
        self.X = copy.deepcopy(X_pred + np.matmul(K, Zi))
        
        #set back the flags to False
        self.new_imu = False
        self.new_rtk = False
        self.new_rpy_imu = False
    
    #function to get position of the rtk from lat,long to the base_link in meters
    def get_baselink_position_from_rtk_in_utm(self, rtk):
        x,y = utm.from_latlon(rtk.latitude, rtk.longitude)[:2]
        z = rtk.altitude
        if self.listener.canTransform(self.robot_frame, self.gps_frame, rospy.Time(0)):
            (trans,quat) = self.listener.lookupTransform(self.robot_frame, self.gps_frame, rospy.Time(0))
            x += trans[0]
            y += trans[1]
            z += trans[2]
            return [x,y,z]
        else:
            return[x,y,z]

    #convert the imu from enu to ned
    def rpy_enu_from_ned(self, vector):
        return [vector.x, -vector.y, pi/2-vector.z]
    
    def imuCallback(self, msg):
        self.imu = msg
        self.new_imu = True
    
    def rpyImuCallback(self, msg):
        self.rpy_imu = msg
        self.new_rpy_imu = True

    def rtkCallback(self, msg):
        self.rtk = msg
        self.new_rtk = True
   
   #publishes an Odometry and broadcast the transform 
    def publish_odom(self):
        msg = Odometry()
        msg.header.frame_id = self.odom_frame
        msg.header.stamp = rospy.Time.now()
        translation = (self.X[0,0], self.X[1,0], self.X[2,0])
        msg.pose.pose.position.x = translation[0] 
        msg.pose.pose.position.y = translation[1] 
        msg.pose.pose.position.z = translation[2] 
        quat = tf.transformations.quaternion_from_euler(0,0,self.X[2,0])
        msg.pose.pose.orientation.x = quat[0]
        msg.pose.pose.orientation.y = quat[1]
        msg.pose.pose.orientation.z = quat[2]
        msg.pose.pose.orientation.w = quat[3]

        #visualize the ellipses
        cov = np.zeros((6,6))
        cov = copy.deepcopy(self.P[:6,:6])
        msg.pose.covariance = cov.flatten().tolist()        

        if not self.odom_as_parent: 
            #we want to publish the tf as child of base_link, because we have multiple odom frames:
            #we broadcast the inverse transform
            transform = tf.transformations.concatenate_matrices(tf.transformations.translation_matrix(translation), tf.transformations.quaternion_matrix(quat))
            inverse = tf.transformations.inverse_matrix(transform)
            t = tf.transformations.translation_from_matrix(inverse) 
            q = tf.transformations.quaternion_from_matrix(inverse)
            self.broadcaster.sendTransform(t, q, msg.header.stamp, self.odom_frame, self.robot_frame)
        else: 
            ##we broadcast the transform with odom as a parent of the robot_frame:
            self.broadcaster.sendTransform(translation, quat, msg.header.stamp, self.robot_frame, self.odom_frame)
        
        self.fuse_odom_pub.publish(msg)

    
    #publishes only the speeds in the robot_frame (as an Odom)    
    def publish_speeds(self):
        #we transform the speed from odom into the robot frame
        vlin = self.X[3,0] * cos(self.X[2,0]) + self.X[4,0]*sin(self.X[2,0])
        vtrans = - self.X[3,0] * sin(self.X[2,0]) + self.X[4,0]*cos(self.X[2,0])
        
        msg = Odometry()
        msg.header.frame_id = self.robot_frame
        msg.header.stamp = rospy.Time.now()
        msg.twist.twist.linear.x = vlin
        msg.twist.twist.linear.y = vtrans
        msg.twist.twist.angular.z = self.X[5,0]
        self.speed_pub.publish(msg)


    #keep the node alive
    def run(self):
        rate = rospy.Rate(20)
        while not rospy.is_shutdown():
            #predict and correct at 20Hz
            X_pred, P_pred = self.predict_without_cmd()
            self.correct(X_pred, P_pred)
            #publish an Odometry
            self.publish_odom()
            self.publish_speeds()
            rate.sleep()

if __name__ == '__main__':
    loc = EKFLocalization()
    loc.run()





